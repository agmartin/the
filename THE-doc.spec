Summary: The Hessling Editor Documentation and macros
Name: THE-doc
Version: 3.3B4
Release: 1
License: GPL
Group: Applications/Editors
Source: THE-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-root
Vendor: Mark Hessling
Packager: Mark Hessling
URL: http://hessling-editor.sourceforge.net
Prefix: /usr
Icon: the64.xpm

%description
This package contains the documentation and sample macros for THE; The Hessling Editor

THE is a full-screen character mode text editor based on the VM/CMS editor
XEDIT and many features of KEDIT written by Mansfield Software.

THE uses Rexx as its macro language which provides a rich and powerful
capability to extend THE.

This port is a native X11 application, and requires the X11 port of PDCurses;
XCurses to function.

For more information on THE, visit http://hessling-editor.sourceforge.net/

For more information on Rexx, visit http://www.rexxla.org

For more information on PDCurses, visit http://pdcurses.sourceforge.net/
%prep

%setup -n THE-%{version}

%build
./configure --prefix=%{prefix} --with-rexx=rexxtrans --with-ncurses
make the.man THE_Help.txt helpviewer

%install
rm -fr %{buildroot}
make DESTDIR=%{buildroot} installdoc

%files
/usr/share/THE
%doc /usr/share/man/man1/the.1.gz
%doc COPYING
%doc README
%doc TODO
%doc index.html
%doc misc
%doc images
%doc commset
%doc commsos
%doc comm
