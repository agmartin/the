Summary: The Hessling Editor
Name: THE-X11
Version: 3.3B4
Release: 1
License: GPL
Group: Applications/Editors
Source: THE-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-root
Vendor: Mark Hessling
Packager: Mark Hessling
URL: http://hessling-editor.sourceforge.net
Prefix: /usr
Icon: the64.xpm
Requires: RexxTrans, PDCurses

%description
THE is a full-screen character mode text editor based on the VM/CMS editor
XEDIT and many features of KEDIT written by Mansfield Software.

THE uses Rexx as its macro language which provides a rich and powerful
capability to extend THE.

This port is a native X11 application, and requires the X11 port of PDCurses;
XCurses to function.

For more information on THE, visit http://hessling-editor.sourceforge.net/

For more information on Rexx, visit http://www.rexxla.org

For more information on PDCurses, visit http://pdcurses.sourceforge.net/
%prep

%setup -n THE-%{version}

%build
./configure --prefix=%{prefix} --with-rexx=rexxtrans --with-xcurses
make xthe THE_Help.txt

%install
rm -fr %{buildroot}
make DESTDIR=%{buildroot} installrpm

%files
/usr/bin/xthe

%post
echo ""
echo "The binary for THE has been installed as /usr/bin/xthe"
echo "to allow the ncurses binary 'nthe' to be installed concurrently."
echo "Create a symbolic link called 'the' to either 'xthe' or 'nthe'"
echo "depending on your preference."
echo ""
