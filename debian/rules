#!/usr/bin/make -f
# Sample debian/rules that uses debhelper.
# GNU copyright 1997 to 1999 by Joey Hess.

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

# Include quilt stuff.
include /usr/share/quilt/quilt.make

# These are used for cross-compiling and for saving the configure script
# from having to guess our platform (since we know it already)
DEB_HOST_GNU_TYPE  ?= $(shell dpkg-architecture -qDEB_HOST_GNU_TYPE)
DEB_BUILD_GNU_TYPE ?= $(shell dpkg-architecture -qDEB_BUILD_GNU_TYPE)

# THE checks the kernel to know if system is 32 or 64 bit. However,
# is sometimes possible to run a 32 bit system using a 64 bit kernel,
# e.g. i386 chroot build box inside a amd64 system.
# Better ask dpkg-architecture about pointer size (in bits) of the
# build environment rather tan trusting the kernel check of host box.
DEB_BUILD_ARCH_BITS ?= $(shell dpkg-architecture -qDEB_BUILD_ARCH_BITS)

# Use default buildflags for hardening
CFLAGS             := $(shell dpkg-buildflags --get CFLAGS) -Wall -g
CPPFLAGS           := $(shell dpkg-buildflags --get CPPFLAGS)
LDFLAGS            := $(shell dpkg-buildflags --get LDFLAGS)

# Make sure we do not link against unneeded libs.
LDFLAGS            += -Wl,--as-needed

export CFLAGS CPPFLAGS LDFLAGS

ifeq (,$(findstring nostrip,$(DEB_BUILD_OPTIONS)))
	INSTALL_PROGRAM += -s
endif

DESTDIR   = $(CURDIR)/debian/tmp
SHAREDIR  = $(DESTDIR)/usr/share/THE
ETCDIR    = $(DESTDIR)/etc
XRESDIR   = $(DESTDIR)/etc/X11/Xresources

update-config: update-config-stamp
update-config-stamp:
	-test -r amd-store.tgz || tar -cvzf amd-store.tgz config.sub config.guess
	-test -r /usr/share/misc/config.sub && \
	  cp -f /usr/share/misc/config.sub config.sub
	-test -r /usr/share/misc/config.guess && \
	  cp -f /usr/share/misc/config.guess config.guess

	touch $@

xcurses-configure: xcurses-configure-stamp
xcurses-configure-stamp: update-config-stamp configure
	dh_testdir

	# Add here commands to configure the package.
	./configure --host=$(DEB_HOST_GNU_TYPE) \
		--build=$(DEB_BUILD_GNU_TYPE) \
		--prefix=/usr \
		--enable-$(DEB_BUILD_ARCH_BITS)bit \
		--mandir=\$${prefix}/share/man \
		--infodir=\$${prefix}/share/info \
		--with-rexx=regina \
		--with-rexxincdir=/usr/include/regina \
		--with-xcurses

	touch $@

ncurses-configure: ncurses-configure-stamp
ncurses-configure-stamp: update-config-stamp configure
	dh_testdir
	[ ! -f Makefile ] ||  $(MAKE) distclean

	# Add here commands to configure the package.
	./configure --host=$(DEB_HOST_GNU_TYPE) \
		--build=$(DEB_BUILD_GNU_TYPE) \
		--prefix=/usr \
		--enable-$(DEB_BUILD_ARCH_BITS)bit \
		--mandir=\$${prefix}/share/man \
		--infodir=\$${prefix}/share/info \
		--with-rexx=regina \
		--with-rexxincdir=/usr/include/regina \
		--with-ncurses

	touch $@

build: build-arch build-indep
build-arch: build-stamp
build-indep: build-stamp
build-stamp: build-ncurses build-xcurses build-doc-stamp

build-xcurses: build-xcurses-stamp
build-xcurses-stamp: patch xcurses-configure-stamp
	dh_testdir

	# Add here commands to compile the package.
	$(MAKE) xthe
	#/usr/bin/docbook-to-man debian/the.sgml > the.1

	touch $@

build-ncurses: build-ncurses-stamp
build-ncurses-stamp: patch ncurses-configure-stamp
	dh_testdir
	# Add here commands to compile the package.
	$(MAKE) nthe

build-doc: build-doc-stamp
build-doc-stamp:
	# Build documentation. manext needs to be available from doc dir
	( cd doc && ln -sf ../manext )
	$(MAKE) THE_Help.txt pdf

	touch $@

# Reverse sort xcurses ncurses call wrt build target. First flavour to
# install must be last built to match configure flavor.
# Remember that builders call build first, then install.
# FIXME: Can we have a more elegant solution?
install: install-xcurses-stamp install-ncurses-stamp install-debian-the-extras build-doc-stamp
	dh_movefiles -pthe-xcurses
	dh_movefiles -pthe-ncurses
	dh_movefiles -pthe-common

	touch $@

install-debian-the-extras:
	mkdir -p $(SHAREDIR)
	install -m 644 debian/local-amd/kedit40.the $(SHAREDIR)
	install -m 644 debian/local-amd/dummy.local-therc $(SHAREDIR)
	install -m 644 debian/local-amd/lynxhlp.the $(SHAREDIR)
	mkdir -p $(ETCDIR)
	install -m 644 debian/local-amd/therc $(SHAREDIR)
	mkdir -p $(XRESDIR)
	install -m 644 debian/the.Xresources $(XRESDIR)/the-xcurses

	touch $@

install-xcurses: install-xcurses-stamp
install-xcurses-stamp: build-xcurses-stamp
	dh_testdir
	dh_testroot
	dh_prep
	dh_installdirs

#	 Add here commands to install the package into debian/the-.
	$(MAKE) install DESTDIR=$(DESTDIR)
#
	mkdir -p $(SHAREDIR)
	install -m 644 *.xbm *.xpm $(SHAREDIR)

	touch $@

install-ncurses: install-ncurses-stamp
install-ncurses-stamp: build-ncurses-stamp
	dh_testdir
	dh_testroot
	install -m 755 nthe $(DESTDIR)/usr/bin/

	touch $@

# Build architecture-independent files here.
binary-indep: install
	dh_testdir -i
	dh_testroot -i
	dh_installdocs -i
	dh_installman -i
	dh_installchangelogs -i
	dh_link -i
	dh_strip -i
	dh_compress -i
	dh_fixperms -i
	dh_installdeb -i
	dh_shlibdeps -i
	dh_gencontrol -i
	dh_md5sums -i
	dh_builddeb -i

# Build architecture-dependent files here.
binary-arch: install
	dh_testdir -a
	dh_testroot -a
	dh_installdocs -a
	dh_installexamples -a
	dh_installman -a
	dh_installinfo -a
	dh_installchangelogs -a
	dh_link -a
	dh_strip -a
	dh_compress -a
	dh_fixperms -a
	dh_installdeb -a
	dh_shlibdeps -a
	dh_gencontrol -a
	dh_md5sums -a
	dh_builddeb -a

binary: binary-indep binary-arch

clean: preclean unpatch
preclean:
	dh_testdir
	dh_testroot
	rm -f update-config-stamp ncurses-configure-stamp xcurses-configure-stamp
	rm -f build-xcurses-stamp build-ncurses-stamp build-doc-stamp
	rm -f install install-xcurses install-ncurses
	rm -f install-debian-the-extras
	rm -rf doc/pdf
	rm -f THE_Help_head THE_Help_set THE_Help_sos THE_Help_tail
	rm -f app5.htm app6.htm title.htm fdl.htm
	rm -f divbot.gif divtop.gif the64.gif
	rm -f config.h defines.h preparecomb.the xthe nthe
	rm -f THE_Help.txt config.guess config.sub the256.gif
	-test -r amd-store.tgz && tar -xvzf amd-store.tgz
	rm -f amd-store.tgz
	rm -f doc/manext

	# Add here commands to clean up after the build process.
	[ ! -f Makefile ] ||  $(MAKE) distclean

	dh_clean

.PHONY: build clean binary-indep binary-arch binary install build-ncurses build-xcurses
