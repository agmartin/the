Summary: The Hessling Editor
Name: THE-ncurses
Version: 3.3B4
Release: 1
License: GPL
Group: Applications/Editors
Source: THE-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-root
Vendor: Mark Hessling
Packager: Mark Hessling
URL: http://hessling-editor.sourceforge.net
Prefix: /usr
Icon: the64.xpm
Requires: RexxTrans, ncurses-devel

%description
THE is a full-screen character mode text editor based on the VM/CMS editor
XEDIT and many features of KEDIT written by Mansfield Software.

THE uses Rexx as its macro language which provides a rich and powerful
capability to extend THE.

This port executes in text mode and can be run from within an xterm window
or over a telnet connection. It requires ncurses to function.

For more information on THE, visit http://hessling-editor.sourceforge.net/

For more information on Rexx, visit http://www.rexxla.org
%prep

%setup -n THE-%{version}

%build
./configure --prefix=%{prefix} --with-rexx=rexxtrans --with-ncurses
make nthe THE_Help.txt

%install
rm -fr %{buildroot}
make DESTDIR=%{buildroot} installrpm

%files
/usr/bin/nthe

%post
echo ""
echo "The binary for THE has been installed as /usr/bin/nthe"
echo "to allow the X11 binary 'xthe' to be installed concurrently."
echo "Create a symbolic link called 'the' to either 'xthe' or 'nthe'"
echo "depending on your preference."
echo ""
